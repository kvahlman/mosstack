__all__ = ["Maximum", "Minimum", "Mean", "Median", "SigmaClip", "SigmaMedian"]

from . import *

Maximum = Maximum.Maximum
Minimum = Minimum.Minimum
Mean = Mean.Mean
Median = Median.Median
SigmaClip = SigmaClip.SigmaClip
SigmaMedian = SigmaMedian.SigmaMedian
